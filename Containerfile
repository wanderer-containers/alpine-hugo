# syntax=docker/dockerfile-upstream:master-labs
FROM docker.io/library/golang:1.20.5-alpine3.18 AS hugobuild

ENV HUGO_VERSION 0.115.3

# this could be so much simpler..
# ref: https://docs.docker.com/engine/reference/builder/#adding-a-git-repository-add-git-ref-dir
# ADD --keep-git-dir=true https://github.com/gohugoio/hugo.git#v${HUGO_VERSION} /hugo

# hadolint ignore=3018
RUN apk add --no-cache git musl-dev gcc g++ && \
    git clone https://github.com/gohugoio/hugo.git /hugo

WORKDIR /hugo

RUN git checkout "v${HUGO_VERSION}" && \
    \
    CGO_ENABLED=1 GOFLAGS="-buildmode=pie -trimpath -mod=readonly -modcacherw" \
    go build -v -tags extended -ldflags '-s -w -extldflags "-static-pie"' \
    && ./hugo version

FROM docker.io/library/alpine:3.18.2

ARG BUILD_DATE
ARG VCS_REF

LABEL description="Container image with Hugo static site generator (extended version) on Alpine base."
LABEL org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.vcs-url="https://git.dotya.ml/wanderer-containers/alpine-hugo.git" \
      org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.license=GPL-3.0-or-later

# required for hugo
# hadolint ignore=3018
RUN apk add --no-cache git

COPY --from=hugobuild /hugo/hugo /usr/bin/hugo
CMD ["/usr/bin/hugo"]

#  vim: set ts=4 ft=dockerfile fenc=utf-8 ff=unix :
