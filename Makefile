dcmd = podman
dfile = Containerfile
dtag = immawanderer/alpine-hugo:testbuild
dargs = build -t $(dtag) $(buildargs) --no-cache --pull - < $(dfile)
buildargs = --build-arg BUILD_DATE=$(build_date) --build-arg VCS_REF=$(vcs_ref)
cleanargs = image rm -f $(dtag)
pruneargs = system prune -af
dargskaniko = run --rm -it -w=$(kanikowdir) -v $$PWD:$(kanikowdir):z
kanikoexecutorimg = gcr.io/kaniko-project/executor:v1.9.0-debug
kanikowdir = /src
kanikocontext = .
kanikoargs = -f=$(dfile) -c=$(kanikocontext) --use-new-run --snapshotMode=redo $(buildargs) --no-push
vcs_ref = $$(git rev-parse --short HEAD || echo development)
build_date= $$(date -u +"%Y-%m-%dT%H:%M:%SZ")
hadolintimg = docker.io/hadolint/hadolint
hadolinttag = v2.12.0-alpine
hadolintargs = run --rm -i -v $$PWD:/src:z --workdir=/src

.PHONY: hadolint build kaniko clean test prune

hadolint:
	$(dcmd) $(hadolintargs) $(hadolintimg):$(hadolinttag) < $(dfile)

kaniko:
	$(dcmd) $(dargskaniko) $(kanikoexecutorimg) $(kanikoargs)

build:
	$(dcmd) $(dargs)

clean:
	$(dcmd) $(cleanargs)

test: hadolint build kaniko

prune:
	$(dcmd) $(pruneargs)
