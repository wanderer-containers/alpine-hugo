# [alpine-hugo](https://git.dotya.ml/wanderer-containers/alpine-hugo)

[![Build Status](https://drone.dotya.ml/api/badges/wanderer-containers/alpine-hugo/status.svg?ref=refs/heads/development)](https://drone.dotya.ml/wanderer-containers/alpine-hugo)
[![Docker Image Version](https://img.shields.io/docker/v/immawanderer/alpine-hugo/linux-amd64)](https://hub.docker.com/r/immawanderer/alpine-hugo/tags/?page=1&ordering=last_updated&name=linux-amd64)
[![Docker Image Size (tag)](https://img.shields.io/docker/image-size/immawanderer/alpine-hugo/linux-amd64)](https://hub.docker.com/r/immawanderer/alpine-hugo/tags/?page=1&ordering=last_updated&name=linux-amd64)
[![Docker pulls](https://img.shields.io/docker/pulls/immawanderer/alpine-hugo)](https://hub.docker.com/r/immawanderer/alpine-hugo/)

This repository provides a Containerfile to effortlessly get a container image containing statically linked [Hugo](https://gohugo.io) static site generator (extended version) on top of minimal [Alpine](https://alpinelinux.org/) base.

The image is rebuilt weekly in CI and automatically pushed to [DockerHub](https://hub.docker.com/r/immawanderer/alpine-hugo).

The project lives at [this Gitea instance](https://git.dotya.ml/wanderer-containers/alpine-hugo).

### What you get
* `hugo-<x>.<y>.<z>+extended` (latest stable version listed on [GitHub](https://github.com/gohugoio/hugo/releases)), freshly built from sources.

### Purpose
* container image containing `Hugo` extended version, suitable for CI.

### License
GPL-3.0-or-later (see [LICENSE](LICENSE) for details).
